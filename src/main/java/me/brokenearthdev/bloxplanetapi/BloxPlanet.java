/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi;

import me.brokenearthdev.bloxplanetapi.entities.Entity;
import me.brokenearthdev.bloxplanetapi.events.Event;
import me.brokenearthdev.bloxplanetapi.events.manager.EventManager;
import me.brokenearthdev.bloxplanetapi.net.BloxPlanetConnection;
import me.brokenearthdev.bloxplanetapi.section.BloxPlanetEvent;
import me.brokenearthdev.bloxplanetapi.section.EnumSection;
import me.brokenearthdev.bloxplanetapi.section.Leaderboard;
import me.brokenearthdev.bloxplanetapi.section.Section;
import me.brokenearthdev.simpleyaml.api.Yaml;
import me.brokenearthdev.bloxplanetapi.entities.LeaderboardEntry;
import me.brokenearthdev.bloxplanetapi.entities.Factory;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Contains functions for getting data from leaderboards, and events.
 *
 * This also contains {@link #getEventManager()} per instance, which is used
 * to manage events and call them. To register an event listener, use
 * {@link EventManager#registerEventListener(Object)}. When an event is called
 * using {@link EventManager#callEvent(Event)}, all registered listeners will
 * have their methods searched for event methods that are suitable to be called
 * depending on the event type.
 * To create an instance of {@link BloxPlanet}, use {@link Factory#newBloxPlanet()}.
 * Creating an instance of {@link BloxPlanet} might cost time, however. It might take
 * seconds to initialize it. But once it is initialized, getting data would not cost
 * time.
 * When {@link BloxPlanet} is initialize, it would search for data and cache them. For
 * every section there is, there will be a {@link BloxPlanetConnection} to it. Using
 * {@link BloxPlanetConnection}, we can get the data in a section and cache it.
 * BloxPlanet is an entity.
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see Leaderboard
 * @see BloxPlanetEvent
 */
public interface BloxPlanet extends Entity {

    /**
     * Gets the {@link EventManager} for the {@link BloxPlanet} instance.
     *
     * An {@link EventManager} contains certain methods for registering,
     * unregistering, and calling events.
     * Events that will be called requires a {@link BloxPlanet} instance
     * required.
     * Each {@link BloxPlanet} instance will have a different {@link EventManager}
     * instance
     *
     * @return The {@link EventManager}
     */
    @NotNull
    EventManager getEventManager();

    /**
     * Gets the {@link BloxPlanetConnection} used when loading a section.
     *
     * Establishing a section using {@link BloxPlanetConnection#establish()}
     * will not change the current data stored in this object. You can refresh
     * the data using {@link #reloadSection(EnumSection)} or {@link #reloadAll()}
     * to reload all.
     * These methods' function is equivalent of establishing a new connection.
     *
     * @param section The section type
     * @return The connection for the specified section type. Please be aware
     * that it won't return a new instance of {@link BloxPlanetConnection}
     */
    @Nullable
    @Contract("null -> null")
    BloxPlanetConnection getConnection(EnumSection section);

    /**
     * Gets the data in YAML syntax and returns it.
     *
     * A {@link Yaml} object houses data stored using the path-value
     * system. It is really recommended to know the basic YAML syntax.
     * Changing the data will not affect any section data
     *
     * @return The data in the YAML syntax
     */
    @NotNull
    Yaml getData();

    /**
     * Gets the section whose type is equal to the required section
     * type.
     *
     * {@link Leaderboard} and {@link BloxPlanetEvent} are examples
     * of a section. If you specify, for example, {@link EnumSection#EVENT}
     * in the parameter, you can cast {@link BloxPlanetEvent} to it
     *
     * @param section The section type
     * @return The section whose type is equal to the specified section type
     * in the parameter
     */
    @Nullable
    @Contract("null -> null")
    Section getSection(EnumSection section);

    /**
     * Gets the leaderboard retrieved for the website.
     *
     * A {@link Leaderboard} object contains data retrieved from the leaderboard
     * section in the website. A {@link Leaderboard} is a section, and therefore,
     * can be retrieved from {@link #getSection(EnumSection)} where {@code EnumSection}
     * in the parameter is set to {@link EnumSection#LEADERBOARD}. You can retrieve
     * names by position or entries by name. A {@link LeaderboardEntry} contains
     * position and name in each object.
     *
     * @return The leaderboard retrieved from the website
     */
    @NotNull
    Leaderboard getLeaderboard();

    /**
     * Gets the current event.
     *
     * A {@link BloxPlanetEvent} is an event that last for a certain period of time
     * before it ends. A {@link BloxPlanetEvent} is a section, and therefore, can
     * be retrieved from {@link #getSection(EnumSection)} where {@code EnumSection}
     * in the parameter is set to {@link EnumSection#EVENT}. You can retrieve the
     * event name. The data is retrieved from the website in the event section.
     *
     * @return The current event retrieved from the website
     */
    @NotNull
    BloxPlanetEvent getCurrentEvent();

    /**
     * Reloads all section data from the website and updates the data.
     *
     * The time that it will take to be reloaded is equivalent to the time it took
     * while initializing the object, which means that it will take seconds until
     * the section is fully reloaded. Reloading a section will update all section data.
     */
    void reloadAll();

    /**
     * Reloads a single section data from the website and updates the specified section data.
     *
     * This can take seconds to update. The section that will be reloaded depends on the
     * section type specified in the parameter. For example, if {@link EnumSection#LEADERBOARD}
     * is specified in the parameter, the method will reload the leaderboard section data.
     *
     * @param section The section type. The section that will be updated depends on the
     *                section type specified.
     */
    @Contract("null -> fail")
    void reloadSection(EnumSection section);
}