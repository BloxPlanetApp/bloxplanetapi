/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.entities;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.section.Leaderboard;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * An Entity represents object types that have different functions
 * and data such as {@link Leaderboard} and {@link BloxPlanet}.
 *
 * All {@link BloxPlanet} objects own the same entity types but
 * with different objects
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see BloxPlanet
 * @see Serializable
 */
public interface Entity extends Serializable {

    /**
     * Gets a BloxPlanet instance that owns this entity
     * object.
     *
     * Different entity objects for the same types can be owned by a
     * different {@link BloxPlanet} object
     *
     * @return A {@link BloxPlanet} instance that owns this
     * entity object
     */
    @NotNull
    BloxPlanet getBloxPlanet();

}
