/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.entities;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.impl.BloxPlanetImpl;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * This class is responsible for creating new {@link BloxPlanet} objects.
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see BloxPlanet
 */
public class Factory {

    /**
     * Creates a new {@link BloxPlanet} object. This takes seconds until
     * the object is fully created.
     *
     * @return The new {@link BloxPlanet} object
     */
    @NotNull
    @Contract(" -> new")
    public static BloxPlanet newBloxPlanet() {
        return new BloxPlanetImpl();
    }

}
