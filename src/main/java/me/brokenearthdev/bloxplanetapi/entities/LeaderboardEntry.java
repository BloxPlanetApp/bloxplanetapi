/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.entities;

import me.brokenearthdev.bloxplanetapi.section.Leaderboard;
import org.jetbrains.annotations.NotNull;

/**
 * A leaderboard entry is an entry in the leaderboard that identifies
 * a name by the position.
 *
 * A {@link LeaderboardEntry} is a {@link Unit} meaning that when you
 * iterate {@link Leaderboard}, you will get {@link LeaderboardEntry}
 * each time ({@code for (LeaderboardEntry e : leaderboardObj) ...})
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see Unit
 * @see Leaderboard
 */
public interface LeaderboardEntry extends Unit {

    /**
     * Gets the position in the entry.
     *
     * @return The position
     */
    int getPosition();

    /**
     * Gets the name in the entry.
     *
     * @return The name
     */
    @NotNull
    String getName();

}
