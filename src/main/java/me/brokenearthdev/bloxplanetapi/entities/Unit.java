/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.entities;

import me.brokenearthdev.bloxplanetapi.section.Leaderboard;

/**
 * A Unit is a building block for some entities. A unit is commonly
 * an iterator type.
 *
 * Inherit from this interface to get a unit that might be used by
 * an entity as an iterator type.
 * For example, {@link Leaderboard} is an entity type that has a unit
 * which is {@link LeaderboardEntry} as its iteration type.
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @see Entity
 */
public interface Unit extends Entity {
}
