/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.events.parse;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.events.Event;
import me.brokenearthdev.bloxplanetapi.parser.Parser;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * This event is called when a parser for a section is done parsing.
 *
 * A {@link Parser} parses a section and converts the data to the
 * {@link Yaml} format.
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see Parser
 * @see Event
 */
public class PostParseEvent extends Event {

    /**
     * The parser that is done parsing a section.
     *
     * This object is initialized by the constructor.
     */
    private Parser parser;

    /**
     * This constructor requires a {@link BloxPlanet} and a {@link Parser}
     * object.
     *
     * If {@code null} is specified in one of the parameters, or both of them,
     * an exception will be thrown.
     *
     * @param planet The owning {@link BloxPlanet} object.
     * @param parser The {@link Parser} that is done parsing.
     */
    @Contract("null, null -> fail; !null, null -> fail; null, !null -> fail")
    public PostParseEvent(@NotNull BloxPlanet planet, @NotNull Parser parser) {
        super(planet);
        Objects.requireNonNull(parser);
        this.parser = parser;
    }

    /**
     * Gets the parser that is done parsing a section.
     *
     * @return The parser that is done parsing a section.
     */
    @NotNull
    public Parser getParser() {
        return parser;
    }

}
