/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.events;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.entities.Entity;
import me.brokenearthdev.bloxplanetapi.events.manager.EventManager;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * An {@link Event} is a superclass of all events.
 *
 * When an event method listens to {@link Event}, it will get
 * called every time when an event is called.
 *
 * An {@link Event} is an {@link Entity}
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see EventManager#callEvent(Event)
 */
public class Event implements Entity {

    /**
     * A {@link BloxPlanet} instance that every {@link Event} object
     * require.
     *
     * This is initialized by the constructor.
     */
    private BloxPlanet planet;

    /**
     * This constructor requires a {@link BloxPlanet} object.
     *
     * If {@code null} is specified in the parameter, an exception
     * is going to be thrown.
     *
     * @param planet A {@link BloxPlanet} object. The {@link BloxPlanet}
     *               object "owns" the event
     */
    @Contract("null -> fail")
    public Event(@NotNull BloxPlanet planet) {
        Objects.requireNonNull(planet);
        this.planet = planet;
    }

    /**
     * Gets the {@link BloxPlanet} for the event.
     *
     * All {@link BloxPlanet} objects own different have a
     * different {@link EventManager} object that have the power
     * to call events. The {@link BloxPlanet} for the event
     * is returned in this method. It is similar to the
     * object that is specified in the constructor
     *
     * @return The owning {@link BloxPlanet}
     */
    @Override
    @NotNull
    public BloxPlanet getBloxPlanet() {
        return planet;
    }

}
