/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.events.load;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.events.Event;
import me.brokenearthdev.bloxplanetapi.loader.Loader;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * This event is called when a loader successfully loaded a
 * section from a given {@link Yaml} object.
 *
 * A {@link Loader} loads a section from a given {@link Yaml}
 * object.
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see Loader
 * @see Event
 */
public class PostLoadEvent extends Event {

    /**
     * The {@link Loader} that successfully loaded a section from
     * a given {@link Yaml} object.
     *
     * This is initialized by the constructor
     */
    private Loader loader;

    /**
     * This constructor requires a {@link BloxPlanet} and a {@link Loader}
     * object.
     *
     * If {@code null} is specified in one of the parameters, or both of them,
     * an exception.
     *
     * @param planet The owning {@link BloxPlanet} object.
     * @param loader The {@link Loader} that successfully loaded a section.
     */
    public PostLoadEvent(@NotNull BloxPlanet planet, @NotNull Loader loader) {
        super(planet);
        Objects.requireNonNull(loader);
        this.loader = loader;
    }

    /**
     * Gets the loader that successfully loaded a section from a given
     * {@link Yaml} object.
     *
     * @return The {@link Loader} that successfully loaded a section.
     */
    @NotNull
    public Loader getLoader() {
        return loader;
    }
}