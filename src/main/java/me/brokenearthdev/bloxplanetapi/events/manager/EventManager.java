/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.events.manager;

import me.brokenearthdev.bloxplanetapi.entities.Entity;
import me.brokenearthdev.bloxplanetapi.events.Event;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * An EventManager object is responsible for registering and unregistering
 * a listener and calling events.
 *
 * A listener is an object that has event methods that might be called when a
 * certain event is called when it is registered. For a method to be an event
 * method, it must meet certain requirements.
 * <ul>
 *     <li>It should be public</li>
 *     <li>It shouldn't be static</li>
 *     <li>It should be annotated with {@link EventHandler}</li>
 *     <li>It should have a parameter requiring an event object or any
 *     object inheriting the {@link Event} class either directly or
 *     indirectly</li>
 * </ul>
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see EventHandler
 */
public interface EventManager extends Entity {

    /**
     * Registers a specified listener object that will have
     * its event method(s) called when an event is called depending
     * on the event's type.
     *
     * @param o A listener object
     */
    void registerEventListener(@NotNull Object o);

    /**
     * Registers an array of listener objects that will have their
     * event method(s) called when an event is called depending on the
     * event's type.
     *
     * @param o A listener array
     */
    void registerEventListener(@NotNull Object[] o);

    /**
     * Registers a list of listener objects that will have their
     * event method(s) called when an event is called depending on
     * the event's type.
     *
     * @param objects A list of objects
     */
    void registerEventListener(@NotNull List<Object> objects);

    /**
     * Unregisters a listener object. That means that their event
     * method(s) won't be called when an event is called. If the object
     * is not registered, nothing will happen.
     *
     * @param o A listener object
     */
    void unregisterEventListener(@NotNull Object o);

    /**
     * Unregisters an array of listener objects. That means that their
     * event method(s) won't be called when an event is called. If an
     * object (or more) is not registered, the method will ignore that
     * object.
     *
     * @param o A listener array
     */
    void unregisterEventListener(@NotNull Object[] o);

    /**
     * Unregisters a list of listener objects. That means that their
     * event method(s) won't be called when an event is called. If an
     * object (or more) is not registered, the method will ignore that
     * object.
     *
     * @param objects A list of listeners
     */
    void unregisterEventListener(@NotNull List<Object> objects);

    /**
     * Calls an event.
     *
     * When an event is called, all registered listeners will
     * have their event method that require that same object type or
     * a superclass of the object type as their parameters called.
     * That can mean that all event methods that require an {@link Event} object will
     * be called because it is a superclass of all events!
     * For a method to be an event method, it should meet certain requirements
     * <ul>
     *     <li>It should be public</li>
     *     <li>It shouldn't be static</li>
     *     <li>It should be annotated with {@link EventHandler}</li>
     *     <li>It should have a parameter requiring an event object or any
     *     object inheriting the {@link Event} class either directly or
     *     indirectly</li>
     * </ul>
     *
     * @param event An event method
     */
    void callEvent(@NotNull Event event);

}