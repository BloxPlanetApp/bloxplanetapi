/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.events.manager;

/**
 * The {@link Priority} of an event method represents the order
 * of event method will be called.
 *
 * First priority to the last priority executed:
 * <ol>
 *     <li>Highest</li>
 *     <li>High</li>
 *     <li>Default</li>
 *     <li>Low</li>
 *     <li>Lowest</li>
 * </ol>
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see EventManager
 * @see EventHandler
 */
public enum Priority {

    /**
     * Methods having the highest priority will get
     * called first.
     */
    HIGHEST,

    /**
     * Methods having the high priority will get
     * called after the highest priority methods.
     */
    HIGH,

    /**
     * Method having the default priority will get called
     * after the methods with the highest priority is called.
     */
    DEFAULT,

    /**
     * Methods having the low priority will get called just before
     * the methods with lowest priority are called.
     */
    LOW,

    /**
     * Methods with the lowest priority will get called last.
     */
    LOWEST

}
