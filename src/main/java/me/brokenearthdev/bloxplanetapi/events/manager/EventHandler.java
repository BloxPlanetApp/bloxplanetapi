/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.events.manager;

import org.jetbrains.annotations.NotNull;
import me.brokenearthdev.bloxplanetapi.events.Event;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotate this annotation with a method that is public and not static
 * that requires an {@link Event} object or any object that inherits from
 * {@link Event} for the method to be called as an event method.
 *
 * A method being annotated with this annotated meets 1 of the requirements
 * for it to become an event method. The requirements for a method to be
 * an event methods are
 * <ul>
 *     <li>A method should be public</li>
 *     <li>A method shouldn't be static</li>
 *     <li>A method should be annotated with this annotation</li>
 *     <li>A method should be requiring an event object or any object that
 *     inherits from the {@link Event} class either directly or indirectly
 *     as their only parameter</li>
 * </ul>
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see Event
 * @see EventManager
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler {

    /**
     * A priority type specifies which event methods get called first when
     * a certain event is called.
     * <p>
     * Event methods with the highest priority get called first and event methods
     * with the lowest priority types get called last.
     * First priority to the last priority executed:
     * <ol>
     *     <li>Highest</li>
     *     <li>High</li>
     *     <li>Default</li>
     *     <li>Low</li>
     *     <li>Lowest</li>
     * </ol>
     * <p>
     * By default, an event method's priority is {@link Priority#DEFAULT}
     *
     * @return The annotated event method's priority
     */
    @NotNull
    Priority priority() default Priority.DEFAULT;
}
