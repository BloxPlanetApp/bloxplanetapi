/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.parser;

import me.brokenearthdev.bloxplanetapi.entities.Entity;
import me.brokenearthdev.bloxplanetapi.section.EnumSection;
import me.brokenearthdev.bloxplanetapi.section.Section;
import me.brokenearthdev.simpleyaml.api.Yaml;
import me.brokenearthdev.bloxplanetapi.net.BloxPlanetConnection;
import me.brokenearthdev.bloxplanetapi.loader.Loader;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.select.Elements;

/**
 * A parser is responsible for parsing the data and converting it
 * into a {@link Yaml} that will be retrieved by a {@link Loader}.
 *
 * A {@link Loader} will then load the data to be able to create
 * a target section's object.
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see Loader
 * @see Yaml
 */
public interface Parser extends Entity {

    /**
     * Gets the section type.
     *
     * An {@link EnumSection} contains constants that all represent
     * different sections. Every constant has a {@link String} attached
     * to it. That {@link String} it the path. You can get the {@link java.net.URL}
     * from a constant by using {@link EnumSection#getURL()}
     *
     * @return The section type
     */
    @NotNull
    EnumSection getSectionType();

    /**
     * Parses the data that is found in each element in {@link #getHousingElements()}
     * and returns the parsed data.
     *
     * The method will call {@link #getHousingElements()} to get the
     * parsed elements and then will begin extracting the data and
     * converting it into a {@link Yaml} object. The returned data
     * is a {@link Yaml} containing text in each element in
     * {@link #getHousingElements()}
     *
     * @return The {@link Yaml} containing data from all elements in
     * {@link #getHousingElements()}
     */
    @Nullable
    Yaml parse();

    /**
     * Looks for the elements that contains essential data and returns it.
     *
     * When this method is called, a {@link java.net.HttpURLConnection} will
     * be established to get the raw data from the website and then will disconnect
     * the {@link BloxPlanetConnection}. The required housing elements will be
     * searched for and will be stored.
     *
     * @return The required housing elements that houses essential data for a
     * specific {@link Section}.
     */
    @Nullable
    Elements getHousingElements();

}
