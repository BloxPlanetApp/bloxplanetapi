/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.parser;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.events.parse.PostParseEvent;
import me.brokenearthdev.bloxplanetapi.events.parse.PreParseEvent;
import me.brokenearthdev.bloxplanetapi.net.BloxPlanetConnection;
import me.brokenearthdev.bloxplanetapi.section.EnumSection;
import me.brokenearthdev.bloxplanetapi.utils.StringUtils;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.Objects;

public class EventParser implements Parser {

    private BloxPlanetConnection connection;

    public EventParser(@NotNull BloxPlanetConnection connection) {
        Objects.requireNonNull(connection);
        this.connection = connection;
    }

    @NotNull
    @Override
    public EnumSection getSectionType() {
        return EnumSection.EVENT;
    }

    @Nullable
    @Override
    public Yaml parse() {
        Elements element = getHousingElements();
        if (element == null)
            return null;
        Element nameElement = element.get(0);
        @NotNull String name = nameElement.text().replace("&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;", "");
        @NotNull Yaml yaml = new Yaml();
        yaml.set("event_name", name);
        yaml.saveToString();
        getBloxPlanet().getEventManager().callEvent(new PostParseEvent(getBloxPlanet(), this));
        return yaml;
    }

    @Nullable
    @Override
    public Elements getHousingElements() {
        try {
            @NotNull HttpURLConnection connection = (HttpURLConnection) this.connection.getURL().openConnection();
            InputStream stream = connection.getInputStream();
            @NotNull String text = StringUtils.getData(stream);
            getBloxPlanet().getEventManager().callEvent(new PreParseEvent(getBloxPlanet(), this));
            Document document = Jsoup.parse(text);
            connection.disconnect();
            Element nameElement = document.getElementById("comp-jmkf4sjd").child(0);
            return new Elements(Arrays.asList(nameElement));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @NotNull
    @Override
    public BloxPlanet getBloxPlanet() {
        return connection.getBloxPlanet();
    }
}
