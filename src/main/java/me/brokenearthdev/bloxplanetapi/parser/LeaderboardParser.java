/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.parser;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.events.parse.PostParseEvent;
import me.brokenearthdev.bloxplanetapi.events.parse.PreParseEvent;
import me.brokenearthdev.bloxplanetapi.net.BloxPlanetConnection;
import me.brokenearthdev.bloxplanetapi.section.EnumSection;
import me.brokenearthdev.bloxplanetapi.utils.StringUtils;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class LeaderboardParser implements Parser {

    private BloxPlanetConnection connection;

    public LeaderboardParser(@Nullable BloxPlanetConnection connection) {
        Objects.requireNonNull(connection);
        this.connection = connection;
    }

    @NotNull
    @Override
    public EnumSection getSectionType() {
        return EnumSection.LEADERBOARD;
    }

    @Nullable
    @Override
    public Yaml parse() {
        @NotNull Yaml yaml = new Yaml();
        @Nullable Elements elements = getHousingElements();
        if (elements == null)
            return null;
        @NotNull LinkedList<String> list = new LinkedList<>();
        for (@NotNull Element element : elements) {
            list.add(element.text().replace("&nbsp; &nbsp; &nbsp;#", "").substring(3));
        }
        yaml.set("leaderboard", list);
        yaml.saveToString();
        getBloxPlanet().getEventManager().callEvent(new PostParseEvent(getBloxPlanet(), this));
        return yaml;
    }

    @Nullable
    @Override
    public Elements getHousingElements() {
        try {
            @NotNull HttpURLConnection connection = (HttpURLConnection) this.connection.getURL().openConnection();
            InputStream in = connection.getInputStream();
            @NotNull String text = StringUtils.getData(in);
            getBloxPlanet().getEventManager().callEvent(new PreParseEvent(getBloxPlanet(), this));
            Document document = Jsoup.parse(text);
            connection.disconnect();
            Element element = document.getElementsByAttributeValue("id", "ab705inlineContent").get(0);
            Elements elements = element.getAllElements();
            @NotNull List<Element> e = new ArrayList<>();
            for (@NotNull Element element1 : elements) {
                if (element1.classNames().contains("txtNew") && !element1.id().equals("comp-jk3ucq7s"))
                    e.add(element1.child(0).child(0));
            }
            return new Elements(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @NotNull
    @Override
    public BloxPlanet getBloxPlanet() {
        return connection.getBloxPlanet();
    }
}
