/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.loader;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.events.load.PostLoadEvent;
import me.brokenearthdev.bloxplanetapi.events.load.PreLoadEvent;
import me.brokenearthdev.bloxplanetapi.exceptions.BloxPlanetException;
import me.brokenearthdev.bloxplanetapi.impl.BloxPlanetEventImpl;
import me.brokenearthdev.bloxplanetapi.section.Section;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.NotNull;

public class EventLoader implements Loader {

    private BloxPlanet planet;
    private String name;
    private Yaml yaml;

    public EventLoader(Yaml yaml, String path, BloxPlanet planet) {
        planet.getEventManager().callEvent(new PreLoadEvent(planet, this));
        if (!yaml.is(path, String.class))
            throw new BloxPlanetException("Data provided is not a string");
        this.name = yaml.getString(path);
        this.planet = planet;
        this.yaml = yaml;
        planet.getEventManager().callEvent(new PostLoadEvent(planet, this));
    }

    @NotNull
    @Override
    public Section newSection() {
        return new BloxPlanetEventImpl(name, yaml, planet);
    }

    @NotNull
    @Override
    public BloxPlanet getBloxPlanet() {
        return planet;
    }
}
