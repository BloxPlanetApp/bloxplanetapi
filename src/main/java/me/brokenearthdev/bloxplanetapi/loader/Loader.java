/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.loader;

import me.brokenearthdev.bloxplanetapi.entities.Entity;
import me.brokenearthdev.bloxplanetapi.parser.Parser;
import me.brokenearthdev.bloxplanetapi.section.Section;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/**
 * A loader loads the data provided as a {@link Yaml} object.
 *
 * This operation is usually quick. The {@link Yaml} data is usually
 * provided by {@link Parser#parse()} which returns a {@link Yaml} object
 * . An example of a loader is {@link LeaderboardLoader}
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see Section
 * @see Parser
 */
public interface Loader extends Entity {

    /**
     * Creates a new {@link Section}.
     *
     * The section data depends on the {@link Yaml} data provided as it
     * copies / converts the data to an object that will be used when initializing
     * a {@link Section}
     *
     * @return Creates a new {@link Section} that has the necessary objects specified
     * in its constructor (if there are any)
     */
    @NotNull
    @Contract(pure = true)
    Section newSection();

}
