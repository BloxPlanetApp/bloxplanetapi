/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.loader;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.entities.LeaderboardEntry;
import me.brokenearthdev.bloxplanetapi.exceptions.BloxPlanetException;
import me.brokenearthdev.bloxplanetapi.impl.LeaderboardEntryImpl;
import me.brokenearthdev.bloxplanetapi.impl.LeaderboardImpl;
import me.brokenearthdev.bloxplanetapi.section.Leaderboard;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class LeaderboardLoader implements Loader {

    @NotNull
    private List<LeaderboardEntry> entries = new ArrayList<>();
    private BloxPlanet planet;
    private Yaml yaml;

    public LeaderboardLoader(Yaml yaml, String path, BloxPlanet planet) {
        if (!yaml.isList(path))
            throw new BloxPlanetException("Data provided is not a list");
        List<String> stringList = yaml.getStringList(path);
        for (int i = 0; i < stringList.size(); i++) {
            entries.add(new LeaderboardEntryImpl(i + 1, stringList.get(i), planet));
        }
        this.planet = planet;
        this.yaml = yaml;
    }

    @NotNull
    @Override
    public Leaderboard newSection() {
        return new LeaderboardImpl(new me.brokenearthdev.bloxplanetapi.list.List<>(entries), yaml, planet);
    }

    @NotNull
    @Override
    public BloxPlanet getBloxPlanet() {
        return planet;
    }
}
