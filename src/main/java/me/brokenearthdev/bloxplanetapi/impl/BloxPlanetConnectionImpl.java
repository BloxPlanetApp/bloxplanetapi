/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.impl;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.exceptions.OperationNotSupportedException;
import me.brokenearthdev.bloxplanetapi.loader.EventLoader;
import me.brokenearthdev.bloxplanetapi.loader.LeaderboardLoader;
import me.brokenearthdev.bloxplanetapi.net.BloxPlanetConnection;
import me.brokenearthdev.bloxplanetapi.parser.EventParser;
import me.brokenearthdev.bloxplanetapi.parser.LeaderboardParser;
import me.brokenearthdev.bloxplanetapi.section.EnumSection;
import me.brokenearthdev.bloxplanetapi.section.Section;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.util.Objects;

class BloxPlanetConnectionImpl implements BloxPlanetConnection {

    private EnumSection section;
    private BloxPlanet planet;

    @Contract("null, null -> fail; !null, null -> fail; null, !null -> fail")
    BloxPlanetConnectionImpl(BloxPlanet planet, EnumSection section) {
        Objects.requireNonNull(planet);
        Objects.requireNonNull(section);
        this.section = section;
        this.planet = planet;
    }

    @SuppressWarnings("all")
    @NotNull
    @Override
    public Section establish() {
        if (section == EnumSection.LEADERBOARD)
            return new LeaderboardLoader(new LeaderboardParser(this).parse(), "leaderboard", planet).newSection();
        else if (section == EnumSection.EVENT)
            return new EventLoader(new EventParser(this).parse(), "event_name", planet).newSection();
        else throw new OperationNotSupportedException("Establishing a connection for a section other than leaderboard is not supported");
    }

    @Override
    @NotNull
    public URL getURL() {
        return section.getURL();
    }

    @Override
    @NotNull
    public BloxPlanet getBloxPlanet() {
        return planet;
    }
}
