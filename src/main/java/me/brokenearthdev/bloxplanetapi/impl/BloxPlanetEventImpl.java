/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.impl;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.section.BloxPlanetEvent;
import me.brokenearthdev.bloxplanetapi.section.EnumSection;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.NotNull;

import java.net.URL;

public class BloxPlanetEventImpl implements BloxPlanetEvent {

    private String name;
    private BloxPlanet planet;
    private Yaml yaml;

    public BloxPlanetEventImpl(@NotNull String name, @NotNull Yaml data, @NotNull BloxPlanet planet) {
        this.name = name;
        this.planet = planet;
        this.yaml = data;
    }

    @NotNull
    @Override
    public String getEventName() {
        return name;
    }

    @NotNull
    @Override
    public URL getURL() {
        return EnumSection.EVENT.getURL();
    }

    @NotNull
    @Override
    public EnumSection getSectionType() {
        return EnumSection.EVENT;
    }

    @NotNull
    @Override
    public BloxPlanet getBloxPlanet() {
        return planet;
    }

    public Yaml getData() {
        return yaml;
    }
}
