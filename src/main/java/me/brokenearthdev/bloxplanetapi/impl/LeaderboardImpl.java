/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.impl;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.list.List;
import me.brokenearthdev.bloxplanetapi.entities.LeaderboardEntry;
import me.brokenearthdev.bloxplanetapi.section.EnumSection;
import me.brokenearthdev.bloxplanetapi.section.Leaderboard;
import me.brokenearthdev.bloxplanetapi.utils.Validity;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class LeaderboardImpl implements Leaderboard {

    private List<LeaderboardEntry> entries;
    private BloxPlanet planet;
    private Yaml yaml;

    public LeaderboardImpl(@NotNull List<LeaderboardEntry> entries, @NotNull Yaml yaml, @NotNull BloxPlanet planet) {
        Objects.requireNonNull(entries);
        Objects.requireNonNull(yaml);
        Objects.requireNonNull(planet);
        Validity.ensureValid(entries);
        this.entries = entries;
        this.planet = planet;
        this.yaml = yaml;
    }

    @Override
    public int getAllEntries() {
        return entries.getSize();
    }

    @NotNull
    @Override
    public List<LeaderboardEntry> getLeaderboardEntries() {
        return entries;
    }

    @Override
    @Nullable
    public LeaderboardEntry getLeaderboardEntryByPosition(int pos) {
        if (pos > entries.getSize() || pos < 1)
            return null;
        @Nullable LeaderboardEntry e = null;
        for (@NotNull LeaderboardEntry entry : entries) {
            if (entry.getPosition() == pos) {
                e = entry;
                break;
            }
        }
        return e;
    }

    @NotNull
    @Override
    public List<LeaderboardEntry> getLeaderboardEntryByName(String name) {
        @NotNull java.util.List<LeaderboardEntry> l = new ArrayList<>();
        for (@NotNull LeaderboardEntry entry : entries) {
            if (entry.getName().equalsIgnoreCase(name)) {
                l.add(entry);
            }
        }
        return new List<>(l);
    }


    @NotNull
    @Override
    public Iterator<LeaderboardEntry> iterator() {
        return entries.iterator();
    }

    @Override
    @NotNull
    public URL getURL() {
        return EnumSection.LEADERBOARD.getURL();
    }

    @NotNull
    @Override
    public EnumSection getSectionType() {
        return EnumSection.LEADERBOARD;
    }

    @NotNull
    @Override
    public BloxPlanet getBloxPlanet() {
        return planet;
    }

    public Yaml getData() {
        return yaml;
    }

}
