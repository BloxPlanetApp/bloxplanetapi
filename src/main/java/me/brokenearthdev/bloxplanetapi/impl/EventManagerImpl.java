/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.impl;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.events.Event;
import me.brokenearthdev.bloxplanetapi.events.manager.EventManager;
import me.brokenearthdev.bloxplanetapi.events.manager.Priority;
import me.brokenearthdev.bloxplanetapi.exceptions.EventListenerExcecption;
import me.brokenearthdev.bloxplanetapi.utils.MethodUtils;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class EventManagerImpl implements EventManager {

    private BloxPlanet planet;

    @NotNull
    private List<Object> objects = new ArrayList<>();

    EventManagerImpl(@NotNull BloxPlanet planet) {
        Objects.requireNonNull(planet);
        this.planet = planet;
    }

    @Override
    public void registerEventListener(@NotNull Object o) {
        Objects.requireNonNull(o);
        objects.add(o);
    }

    @Override
    public void registerEventListener(@NotNull Object[] o) {
        for (Object obj : o) {
            registerEventListener(obj);
        }
    }

    @Override
    public void registerEventListener(@NotNull List<Object> objects) {
        objects.forEach(this::registerEventListener);
    }

    @Override
    public void unregisterEventListener(@NotNull Object o) {
        Objects.requireNonNull(o);
        objects.remove(o);
    }

    @Override
    public void unregisterEventListener(@NotNull Object[] o) {
        for (Object obj : o) {
            unregisterEventListener(obj);
        }
    }

    @Override
    public void unregisterEventListener(@NotNull List<Object> objects) {
        objects.forEach(this::unregisterEventListener);
    }

    @Override
    public void callEvent(@NotNull Event event) {
        @NotNull List<InstanceMethod> highest = new ArrayList<>();
        @NotNull List<InstanceMethod> high = new ArrayList<>();
        @NotNull List<InstanceMethod> def = new ArrayList<>();
        @NotNull List<InstanceMethod> low = new ArrayList<>();
        @NotNull List<InstanceMethod> lowest = new ArrayList<>();
        objects.forEach(o -> {
            for (@NotNull Method method : o.getClass().getMethods()) {
                if (MethodUtils.isEventMethod(method, event)) {
                    if (MethodUtils.getPriority(method, event) == Priority.HIGHEST)
                        highest.add(new InstanceMethod(o, method));
                    else if (MethodUtils.getPriority(method, event) == Priority.HIGH)
                        high.add(new InstanceMethod(o, method));
                    else if (MethodUtils.getPriority(method, event) == Priority.DEFAULT)
                        def.add(new InstanceMethod(o, method));
                    else if (MethodUtils.getPriority(method, event) == Priority.LOW)
                        low.add(new InstanceMethod(o, method));
                    else lowest.add(new InstanceMethod(o, method));
                }
            }
        });
        highest.forEach(i -> i.call(event));
        high.forEach(i -> i.call(event));
        def.forEach(i -> i.call(event));
        low.forEach(i -> i.call(event));
        lowest.forEach(i -> i.call(event));
    }

    @NotNull
    @Override
    public BloxPlanet getBloxPlanet() {
        return planet;
    }

    private static class InstanceMethod {
        Object o;
        private Method method;

        InstanceMethod(Object o, Method method) {
            this.o = o;
            this.method = method;
        }

        public Object getObject() {
            return o;
        }

        public Method getMethod() {
            return method;
        }

        void call(Event event) {
            try {
                method.invoke(o, event);
            } catch (InvocationTargetException | IllegalAccessException e) {
                if (e instanceof InvocationTargetException)
                    throw new EventListenerExcecption("One of the event listeners had an uncaught exception", e);
            }
        }
    }

}
