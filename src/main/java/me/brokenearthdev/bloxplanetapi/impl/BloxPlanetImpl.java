/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.impl;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.events.manager.EventManager;
import me.brokenearthdev.bloxplanetapi.exceptions.OperationNotSupportedException;
import me.brokenearthdev.bloxplanetapi.net.BloxPlanetConnection;
import me.brokenearthdev.bloxplanetapi.section.BloxPlanetEvent;
import me.brokenearthdev.bloxplanetapi.section.EnumSection;
import me.brokenearthdev.bloxplanetapi.section.Leaderboard;
import me.brokenearthdev.bloxplanetapi.section.Section;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class BloxPlanetImpl implements BloxPlanet {

    private EventManager manager;
    private BloxPlanetConnection lb;
    private BloxPlanetConnection e;
    private Yaml data;
    private Leaderboard leaderboard;
    private BloxPlanetEvent event;

    public BloxPlanetImpl() {
        load();
    }

    @NotNull
    @Override
    public EventManager getEventManager() {
        return manager;
    }

    @Nullable
    @Override
    public BloxPlanetConnection getConnection(EnumSection section) {
        if (section == EnumSection.EVENT)
            return e;
        if (section == EnumSection.LEADERBOARD)
            return lb;
        return null;
    }

    @NotNull
    @Override
    public Yaml getData() {
        return data;
    }

    @Nullable
    @Override
    public Section getSection(EnumSection section) {
        if (section == EnumSection.LEADERBOARD)
            return getLeaderboard();
        if (section == EnumSection.EVENT)
            return getCurrentEvent();
        return null;
    }

    @NotNull
    @Override
    public Leaderboard getLeaderboard() {
        return leaderboard;
    }

    @NotNull
    @Override
    public BloxPlanetEvent getCurrentEvent() {
        return event;
    }

    @Override
    public void reloadAll() {
        for (EnumSection section : EnumSection.values()) {
            reloadSection(section);
        }
    }

    @Override
    public void reloadSection(EnumSection section) {
        Objects.requireNonNull(section);
        if (section == EnumSection.EVENT) {
            this.event = (BloxPlanetEvent) e.establish();
            data.set("event_name", ((BloxPlanetEventImpl) event).getData().getString("event_name"));
        } else if (section == EnumSection.LEADERBOARD) {
            this.leaderboard = (Leaderboard) lb.establish();
            data.set("leaderboard", ((LeaderboardImpl) leaderboard).getData().getStringList("leaderboard"));
        } else
            throw new OperationNotSupportedException("Can't reload this section");
        data.saveToString();
    }

    private void load() {
        this.manager = new EventManagerImpl(this);
        this.lb = new BloxPlanetConnectionImpl(this, EnumSection.LEADERBOARD);
        this.e = new BloxPlanetConnectionImpl(this, EnumSection.EVENT);
        this.data = new Yaml();
        this.leaderboard = (Leaderboard) lb.establish();
        this.event = (BloxPlanetEvent) e.establish();
        data.set("leaderboard", ((LeaderboardImpl) leaderboard).getData().getStringList("leaderboard"));
        data.set("event_name", ((BloxPlanetEventImpl) event).getData().getString("event_name"));
        data.saveToString();
    }


    @Override
    public @NotNull BloxPlanet getBloxPlanet() {
        return this;
    }
}
