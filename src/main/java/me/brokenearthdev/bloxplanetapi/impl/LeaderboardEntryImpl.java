/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.impl;

import me.brokenearthdev.bloxplanetapi.BloxPlanet;
import me.brokenearthdev.bloxplanetapi.entities.LeaderboardEntry;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class LeaderboardEntryImpl implements LeaderboardEntry {

    private int position;
    private String name;
    private BloxPlanet planet;

    public LeaderboardEntryImpl(int position, @NotNull String name, @NotNull BloxPlanet planet) {
        Objects.requireNonNull(name);
        Objects.requireNonNull(planet);
        this.position = position;
        this.name = name;
        this.planet = planet;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    @NotNull
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public BloxPlanet getBloxPlanet() {
        return planet;
    }
}
