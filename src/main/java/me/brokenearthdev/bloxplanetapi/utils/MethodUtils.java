/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.utils;

import me.brokenearthdev.bloxplanetapi.events.Event;
import me.brokenearthdev.bloxplanetapi.events.manager.EventHandler;
import me.brokenearthdev.bloxplanetapi.events.manager.Priority;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class MethodUtils {

    public static boolean isEventMethod(Method method, @NotNull Event event) {
        return Modifier.isPublic(method.getModifiers()) && !Modifier.isStatic(method.getModifiers())
                && method.getParameterCount() == 1 && contains(event.getClass(), method.getParameterTypes()[0]) &&
                method.isAnnotationPresent(EventHandler.class);
    }

    private static boolean contains(Class c1, Class c2) {
        return c1.equals(c2) || c1.getSuperclass() != null && contains(c1.getSuperclass(), c2);
    }

    public static Priority getPriority(@NotNull Method method, @NotNull Event event) {
        if (!isEventMethod(method, event))
            return null;
        return method.getAnnotation(EventHandler.class).priority();
    }

}
