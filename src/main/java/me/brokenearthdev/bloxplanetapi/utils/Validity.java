/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.utils;

import me.brokenearthdev.bloxplanetapi.exceptions.BloxPlanetException;
import me.brokenearthdev.bloxplanetapi.list.List;
import me.brokenearthdev.bloxplanetapi.entities.LeaderboardEntry;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class Validity {

    public static void ensureValid(List<LeaderboardEntry> entries) {
        @NotNull java.util.List<Integer> integers = new ArrayList<>();
        entries.forEach(e -> {
            if (integers.contains(e.getPosition()))
                throw new BloxPlanetException("list is not valid as the position exists");
            integers.add(e.getPosition());
        });
    }

}
