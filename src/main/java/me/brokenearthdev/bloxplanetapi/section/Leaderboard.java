/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.section;

import me.brokenearthdev.bloxplanetapi.list.List;
import me.brokenearthdev.bloxplanetapi.entities.LeaderboardEntry;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A {@link Leaderboard} is a section that has its data gathered from the
 * BloxPlanet website in the leaderboard page.
 *
 * You can track leaderboard data and retrieve them. This class also inherits from
 * {@link Iterable}, which means that you can iterate a leaderboard object.
 * For example: {@code for (LeaderboardEntry e : leaderboardObj) ...}
 * A {@link LeaderboardEntry} is an entry in the leaderboard that contains
 * a position and name. You can say that the leaderboard consists of {@link LeaderboardEntry}
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see LeaderboardEntry
 */
public interface Leaderboard extends Iterable<LeaderboardEntry>, Section {

    /**
     * Gets the amount of entries on the leaderboard.
     *
     * The amount of entries is usually equivalent to the
     * last position or the amount of players on the leaderboard.
     *
     * @return The amount of entries on the leaderboard
     */
    int getAllEntries();

    /**
     * Gets all leaderboard entries.
     *
     * Each entry contains a name and position. The amount
     * of the total entries is equivalent to the size of the list.
     *
     * @return All leaderboard entries as a {@link List}
     */
    @NotNull
    List<LeaderboardEntry> getLeaderboardEntries();

    /**
     * Gets a {@link LeaderboardEntry} by position.
     *
     * If the position specified is less than {@code 1} or
     * greater than the entries' size, which is found out by
     * calling {@link #getAllEntries()}, the method will
     * return {@code null}
     *
     * @param pos The position that will have its {@link LeaderboardEntry}
     *            retrieved.
     * @return The {@link LeaderboardEntry} by position. If the position
     * is greater than the total entries size or less than {@code 1},
     * the method will return {@code null}
     */
    @Nullable
    LeaderboardEntry getLeaderboardEntryByPosition(int pos);

    /**
     * Gets a {@link List} of {@link LeaderboardEntry} with their
     * name equal (not case sensitive) to the name specified in
     * the parameter.
     *
     * @param name The name that will be searched in all of the
     *             {@link LeaderboardEntry}. The name in a
     *             {@link LeaderboardEntry} might not have the same
     *             case in the value specified
     * @return A {@link List} of {@link LeaderboardEntry} with their
     * names equal (not case sensitive)
     */
    @NotNull
    List<LeaderboardEntry> getLeaderboardEntryByName(String name);

}
