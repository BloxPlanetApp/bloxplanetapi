/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.section;

import me.brokenearthdev.bloxplanetapi.entities.Entity;
import me.brokenearthdev.bloxplanetapi.loader.Loader;
import me.brokenearthdev.simpleyaml.api.Yaml;
import me.brokenearthdev.bloxplanetapi.parser.Parser;
import org.jetbrains.annotations.NotNull;

import java.net.URL;

/**
 * A section is a page on the BloxPlanet website that has data gathered
 * from it.
 *
 * A section should have a data that will be withdrawn from it.
 * All sections are entities. An example of a section is {@link Leaderboard}.
 * Sections often has a {@link Loader} to newSection the section from a temporary
 * yaml object and a {@link Parser} to get the data from the section in
 * the website and save it as a {@link Yaml} object.
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see EnumSection
 */
public interface Section extends Entity {

    /**
     * This method gets the {@link URL} used to look for the section data.
     *
     * Every section has a {@link URL} that has its data gathered and stored
     * into a section object (or any object that inherit from this interface).
     *
     * @return The {@link URL} used to look for the section data
     */
    @NotNull URL getURL();

    /**
     * Gets the section type.
     *
     * An {@link EnumSection} is an enum that has several constants with each
     * constant representing a section. Each constant has a {@link String}
     * attached. This {@link String} is the path for the section. You can
     * get the {@link URL} from each contant by calling {@link EnumSection#getURL()}
     *
     * @return The section type
     */
    @NotNull EnumSection getSectionType();

}
