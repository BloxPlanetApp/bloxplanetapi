/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.section;

import org.jetbrains.annotations.NotNull;

/**
 * A BloxPlanet event takes place for a certain period of time until
 * it ends.
 *
 * When an event ends, a new event with a different event name
 * will take place.
 * {@link BloxPlanetEvent} is a section. Which means that it has its own
 * data gathered from the BloxPlanet website.
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see Section
 */
public interface BloxPlanetEvent extends Section {

    /**
     * Every BloxPlanet event has a name and takes place for a certain
     * period of time until it ends.
     *
     * Therefore, you can be sure when an event has ended when an
     * event name changed.
     *
     * @return The current event's name
     */
    @NotNull
    String getEventName();

}
