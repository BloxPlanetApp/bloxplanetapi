/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.section;

import me.brokenearthdev.bloxplanetapi.exceptions.BloxPlanetException;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * This enum contains different constants for representing different
 * remote sections.
 *
 * Each constant initializes the constructor with a given path. You can
 * retrieve a {@link URL} from each section by using {@link EnumSection#getURL()}
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see Section
 */
public enum EnumSection {

    /**
     * The leaderboard section.
     */
    LEADERBOARD("https://mahmady05.wixsite.com/bloxplanet/leaderboard"),

    /**
     * The event section.
     */
    EVENT("https://mahmady05.wixsite.com/bloxplanet/event");

    /**
     * The path of a constant.
     */
    private String path;

    /**
     * The retrieved {@link URL} from the {@link #path} of the
     * constant.
     */
    private URL url;

    EnumSection(String path) {
        this.path = path;
        this.url = genURL();
    }

    /**
     * Gets the {@link URL} that is retrieved from the path
     * of the constant.
     *
     * @return The {@link URL} of the constant
     */
    public URL getURL() {
        return url;
    }

    /**
     * Gets the path of the constant and returns it
     *
     * @return The path of the constant
     */
    public String getPath() {
        return path;
    }

    /**
     * Gets the {@link URL} from the path of a constant.
     *
     * This method uses a try-catch block. If an exception
     * is thrown, {@link BloxPlanetException} will be thrown.
     *
     * @return The generated {@link URL}.
     */
    private URL genURL() {
        try {
            return new URL(path);
        } catch (MalformedURLException e) {
            throw new BloxPlanetException(e);
        }
    }

    @Override
    public String toString() {
        return path;
    }

}
