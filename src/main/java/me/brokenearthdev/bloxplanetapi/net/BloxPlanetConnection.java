/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.net;

import me.brokenearthdev.bloxplanetapi.entities.Entity;
import me.brokenearthdev.bloxplanetapi.section.Section;
import me.brokenearthdev.bloxplanetapi.parser.Parser;
import me.brokenearthdev.bloxplanetapi.loader.Loader;
import me.brokenearthdev.simpleyaml.api.Yaml;
import org.jetbrains.annotations.NotNull;

import java.net.URL;

/**
 * A {@link BloxPlanetConnection} is a temporary connection between the
 * local system and the remote host which is established using {@link #establish()}.
 *
 * When establishing a connection, a parser for the section that will be looked for
 * and will start parsing and loading the data. The loaded data will be a {@link Section}.
 * After the parser gets the data from the specified {@link URL} the connection
 * between the local system and the host will stop
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 */
public interface BloxPlanetConnection extends Entity {

    /**
     * Establishes a connection between the local system and the remote
     * host.
     *
     * When this method is called, a new {@link Parser} for the section
     * will be initialized as well as its loader. The {@link Parser} will get
     * the raw data from the remote host and then the connection will disconnect.
     * The parser will then parse the data and then store a data in a {@link Yaml}
     * object. The {@link Loader} will then retrieve the data stored in the
     * {@link Yaml} object and start creating objects that store the data.
     * The objects that is created will then be used when creating the instance
     * of the target section. This operation usually takes few seconds to successfully
     * create a target section instance.
     *
     * @return The new {@link Section} object for the target section type
     */
    @NotNull
    Section establish();

    /**
     * Gets the {@link URL} that will be used when establishing a connection to
     * retrieve a {@link Section} object using {@link #establish()}.
     *
     * @return The {@link URL} that will be used when establishing a connection
     */
    @NotNull
    URL getURL();

}