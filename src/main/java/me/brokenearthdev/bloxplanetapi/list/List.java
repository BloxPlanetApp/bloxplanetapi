/*
Copyright 2018 github.com/BrokenEarthDev && gitlab.com/BrokenEarth

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
 */
package me.brokenearthdev.bloxplanetapi.list;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.*;

/**
 * This class is similar to {@link java.util.List} that has functions
 * for getting an object from an array.
 *
 * However, the class doesn't contain methods for adding and removing
 * objects from it.
 * <p>
 * You can initialize an object of this class using an array or a
 * {@link java.util.List}. Initializing this class with a {@link java.util.List}
 * or an array will have their data copied to a local list.
 * <p>
 * That list will have its data retrieved from when {@link #get(int)} is called.
 *
 * @param <T> The data type
 *
 * @author BrokenEarthDev
 * @version 1.0
 * @since 1.0
 * @see java.util.List
 */
public class List<T> implements Serializable, Iterable<T> {

    /**
     * Contains objects that might be retrieved from.
     *
     * This is initialized by a constructor.
     */
    private java.util.List<T> t;

    /**
     * This constructor requires a {@link java.util.List} of
     * objects with the same type as this object's type parameter.
     *
     * If {@code null} is specified in the parameter, an exception
     * will be thrown.
     *
     * @param t A list of objects with the same type as this
     *          object's type parameter.
     */
    @Contract("null -> fail")
    public List(java.util.List<T> t) {
        Objects.requireNonNull(t);
        this.t = t;
    }

    /**
     * This constructor requires a array of objects with the same type
     * as this object's type parameter.
     *
     * If {@code null} is specified in the parameter, an exception will
     * be thrown.
     *
     * @param t An array of objects with the same type as this
     *          object's type parameter.
     */
    @Contract("null -> fail")
    public List(T[] t) {
        Objects.requireNonNull(t);
        java.util.List<T> list = new ArrayList<>();
        Collections.addAll(list, t);
        this.t = list;
    }

    /**
     * Gets the iterator of this list.
     *
     * @return The iterator of this list.
     */
    @NotNull
    @Override
    public Iterator<T> iterator() {
        return t.iterator();
    }

    /**
     * Gets the object in the list with the index
     * specified.
     *
     * @param i The index.
     * @return The object found in the list with the
     * same index as the index specified.
     */
    public T get(int i) {
        return t.get(i);
    }

    /**
     * Gets the size of the list.
     *
     * @return The size of the list.
     */
    public int getSize() {
        return t.size();
    }

}
